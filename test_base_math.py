import unittest
from base_math import BaseMath


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.base_math = BaseMath()

    def test_add(self):
        self.assertEqual(self.base_math.add(1, 2), 3)

    def test_add_negative(self):
        self.assertEqual(self.base_math.add(-1, -2), -3)

    def test_add_float(self):
        self.assertEqual(self.base_math.add(1.5, 2.5), 4)

    def test_add_string(self):
        self.assertRaises(TypeError, self.base_math.add, 'a', 2)

    def test_sub(self):
        self.assertEqual(self.base_math.sub(1, 2), -1)

    def test_mul(self):
        self.assertEqual(self.base_math.mul(1, 2), 2)

    def test_div(self):
        self.assertEqual(self.base_math.div(1, 2), 0.5)

    def test_avg(self):
        self.assertEqual(self.base_math.avg(1, 2), 1.5)

    def tearDown(self):
        del self.base_math

if __name__ == '__main__':
    unittest.main()
