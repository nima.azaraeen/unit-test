class BaseMath:
    def add(self, x, y):
        if not isinstance(x, (int, float)) or not isinstance(y, (int, float)):
            raise TypeError('Invalid type!')
        return x + y

    def sub(self, x, y):
        if not isinstance(x, (int, float)) or not isinstance(y, (int, float)):
            raise TypeError('Invalid type!')
        return x - y

    def mul(self, x, y):
        if not isinstance(x, (int, float)) or not isinstance(y, (int, float)):
            raise TypeError('Invalid type!')
        return x * y

    def div(self, x, y):
        if not isinstance(x, (int, float)) or not isinstance(y, (int, float)):
            raise TypeError('Invalid type!')
        if y == 0:
            raise ValueError('Can not divide by zero!')
        return x / y

    def avg(self, x, y):
        return (x + y) / 2